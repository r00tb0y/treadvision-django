## TreadVision Django App

This project is a Django-based app for the TredVision project. Hopefully using Django will enable us to quickly prototype and showcase features for a mobile app later on.

### Development setup

### Prerequisites

* Docker
* docker-compose
* python

#### Spinning up containers

For now dev code lives in the develop branch and its branches. So make sure to check those out.

To simply run the app in dev mode on your ocal machine you should be able to simply run

`docker-compose up -d --build`

in the project's root (where the docker-compose.yml lives).
That spins up the defined Docker containers (in docker-compose.yml). The -d flag detaches the shell from it  - so you don't have to keep the current shell running and open another one to continue working. The --build flag tells docker compose to (re-)build the required container. You can then visit the project under `localhost:4000`.

#### Local dev

Although you don't have to, for local development I recommend usuing a python virtual environment (in order to be consistent with versionss and remain independent from your system's python and installed packages). To do so, `cd` into the app dir and run:

`
$ python3.8 -m venv env
`

`
$ source env/bin/activate
`

### TODOs

* Add e.g. Gunicorn and .env.prod for a production setup
* Add features